docker build -t gitlab-runner .
docker run -d --name gitlab-runner \
    --network tp4_network --restart always \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v ./config:/etc/gitlab-runner \
    -v ~/.ssh:/home/gitlab-runner/.ssh \
    gitlab-runner
