# TP4 - Ansible tools
---
## _Gitlab Runner + Fake server_
---

## Features

- Gitlab runner configuration to run a custom runner
- Fake ubuntu server (docker-in-docker)

## Tech

These features are fully developed into *Dockerfile* for training purpose only by Pr. Oussama BOUISFI

## Installation

* Create a common network between containers :
```sh
$ docker network create tp4_network
```
* Clone this project
```sh
$ git clone https://gitlab.com/obo-training/cicd-tp4-ansible-tools.git
$ cd cicd-tp4-ansible-tools
```
---
--- Gitlab Runner ---
---

We have to configure the local gitlab runner to make a connection with our gitlab project.
We create a configuration file `config.toml` from the `config.toml.example` and put :
- a random name of current runner
- the generated token in gitlab.com

```sh
$ cd gitlab-runner
$ cp config/config.toml.example config/config.toml
<<< Update the config/config.toml content >>>
```
 Now we can run the gitlab-runner container :

```sh
$ docker build -t gitlab-runner .
$ docker run -d --name gitlab-runner \
    --network tp4_network --restart always \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v ./config:/etc/gitlab-runner \
    -v ~/.ssh:/home/gitlab-runner/.ssh \
    gitlab-runner
```

---
--- Fake Server ---
---
* Run Fake Ubuntu server locally (Update <ROOT_PASSWORD> with the Ubuntu server password from your choice)
```sh
$ cd fake-server
$ docker build --build-arg ROOT_PASSWORD=<ROOT_PASSWORD> -t fake-server .
$ docker run -d --privileged --name fake-server \
    --network tp4_network \
    -p 3003:3003 \
    -p 22:22 \
    fake-server
```